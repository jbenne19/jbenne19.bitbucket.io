var annotated_dup =
[
    [ "encoder2", null, [
      [ "Encoder", "classencoder2_1_1Encoder.html", "classencoder2_1_1Encoder" ]
    ] ],
    [ "motor", null, [
      [ "DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "ShareMotorInfo", "classshares_1_1ShareMotorInfo.html", "classshares_1_1ShareMotorInfo" ]
    ] ],
    [ "task_encoder_v2", null, [
      [ "Task_Encoder", "classtask__encoder__v2_1_1Task__Encoder.html", "classtask__encoder__v2_1_1Task__Encoder" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_user_v3", null, [
      [ "Task_User", "classtask__user__v3_1_1Task__User.html", "classtask__user__v3_1_1Task__User" ]
    ] ]
];