/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "John Bennett Repository", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Coursework Links", "index.html#sec_links", null ],
    [ "Results/Discussion", "Results_2Discussion.html", [
      [ "Lab 4 Discussion", "Results_2Discussion.html#sec_report", null ]
    ] ],
    [ "HW0x02 & HW0x03: Matlab Ball Balancer Simulation", "HWM.html", [
      [ "HW0x02 & HW0x03: MATLAB Ball Balancer Simulation", "HWM.html#B", null ],
      [ "Hw0x02: Ball Balancing System Modeling", "HWM.html#HW2", null ],
      [ "HW0x03: Ball Balancer Simulation", "HWM.html#HW3", null ],
      [ "Animation", "HWM.html#Animation", null ],
      [ "Calculations", "HWM.html#Hand", null ]
    ] ],
    [ "HW0x03: Python Ball Balancer Simulation", "HW.html", [
      [ "HW 0x03: Ball Balancer Simulation", "HW.html#Ball", null ],
      [ "Simulation", "HW.html#Simulation", null ],
      [ "Plot", "HW.html#Plot", null ],
      [ "Constants", "HW.html#Constants", null ],
      [ "Sim1 Variables Coditions", "HW.html#Sim1", null ],
      [ "Sim2 Variables Coditions", "HW.html#Sim2", null ],
      [ "Sim3 Variables Coditions", "HW.html#Sim3", null ]
    ] ],
    [ "HW0xff: Ball Balancer Term Project", "LABff.html", [
      [ "Task Diagram", "LABff.html#task_diagram", null ],
      [ "FSM Diagrams", "LABff.html#fsm_diagrams", null ],
      [ "User Interface", "LABff.html#User_Interface", null ],
      [ "Controller Selection", "LABff.html#Contrl", null ],
      [ "Data Plots", "LABff.html#Plots", null ],
      [ "Video Demonstration", "LABff.html#Video", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"HW.html",
"main_8py.html#a79845bc10a1438cd0011f900b4605973"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';