var classtask__control_1_1Task__Controller =
[
    [ "__init__", "classtask__control_1_1Task__Controller.html#add9e24873bac17e66f4caa721c511373", null ],
    [ "run", "classtask__control_1_1Task__Controller.html#a4a18c7bfee38364aa95579628bac1651", null ],
    [ "Ball_Data", "classtask__control_1_1Task__Controller.html#a1ba0803834d80c4d84dfa663214e57de", null ],
    [ "C", "classtask__control_1_1Task__Controller.html#ac28f293c1f168081d7d257d4f0f47165", null ],
    [ "D1c", "classtask__control_1_1Task__Controller.html#a36d09835c35ab5d9880318f6373aff50", null ],
    [ "D2c", "classtask__control_1_1Task__Controller.html#a786f057ffff254b76918be38ea674741", null ],
    [ "Duty_S", "classtask__control_1_1Task__Controller.html#a2157ee3b876584d68cdfe85a31880d5e", null ],
    [ "IMU_Data", "classtask__control_1_1Task__Controller.html#ab706638475825a7ee6f82b4e3873bbd7", null ],
    [ "Kp1", "classtask__control_1_1Task__Controller.html#a345e2bf4a51efe06b61e353afb09828d", null ],
    [ "Kp2", "classtask__control_1_1Task__Controller.html#affd0dca84efcb5bb715ec3e6fc74a032", null ],
    [ "Mode", "classtask__control_1_1Task__Controller.html#a161cdf0fd7e715b7e4a873d0d6cc982f", null ],
    [ "next_time", "classtask__control_1_1Task__Controller.html#a0f851f46bf664e43f4d890b4d8dd06a8", null ],
    [ "period", "classtask__control_1_1Task__Controller.html#a063d8615c8d9160f918bdde20047b099", null ],
    [ "State_S", "classtask__control_1_1Task__Controller.html#afd9022a0e426a70a76c02ed80e3e73c7", null ]
];